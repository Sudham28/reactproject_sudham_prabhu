import axios from "axios";

const getPayments = (payments) => {
  return {
    type: "GET_PAYMENTS_SUCCESS",
    payload: payments,
  };
};

const loader = (value) => {
  return {
    type: "LOADER",
    payload: value,
  };
};

const failed = (errorMessage) => {
  return {
    type: "FAILED",
    payload: errorMessage,
  };
};

export const fetchPayments = () => {
  return (despatch, getState) => {
    despatch(loader(true));
    axios
      .get("http://localhost:8080/payment/all")
      .then((res) => {
        setTimeout(() => {
          // setTimeout add to test loader
          despatch(loader(false));
          despatch(getPayments(res.data));
        }, 3000);
      })
      .catch((error) => {
        despatch(loader(false));
        despatch(failed(error.message));
      });
  };
};

const addPaymentFailed = (error) => {
  return {
    type: "ADD_PAYMENT_FAILURE",
    payload: { message: error },
  };
};

export const addPaymnetSuccess = (res) => {
  return {
    type: "ADD_PAYMENT_SUCCESS",
    payload: res,
  };
};

export const addPayments = (paymenrReqObj) => {
  return (despatch, getStates) => {
    despatch(loader(true));
    axios.post("http://localhost:8080/payment/save", paymenrReqObj).then(
      (resp) => {
        despatch(loader(false));
        despatch(addPaymnetSuccess(resp.data));
      },
      (error) => {
        despatch(loader(false));
        despatch(addPaymentFailed(error.message));
      }
    );
  };
};
