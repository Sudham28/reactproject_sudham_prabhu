import { combineReducers } from "redux";
import { AddPaymentReducer } from "./AddPaymentReducer";
import { GetPaymentReducer } from "./GetPaymentReducer";
import { CommonReducer } from "./CommonReducer";

const rootReducer = combineReducers({
  GetPayments: GetPaymentReducer,
  AddPayments: AddPaymentReducer,
  Commons: CommonReducer,
});

export default rootReducer;
