export const CommonReducer = (state = {}, action) => {
  switch (action.type) {
    case "LOADER":
      return { ...state, loader: action.payload };
    case "FAILED":
      return {
        ...state,
        errorMessage: action.payload,
        success: false,
      };
    case "SET_INITIALSTATE":
      return state;
    default:
      return state;
  }
};
