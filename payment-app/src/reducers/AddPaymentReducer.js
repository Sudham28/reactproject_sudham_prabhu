export const AddPaymentReducer = (state = { success: false }, action) => {
  switch (action.type) {
    case "ADD_PAYMENT_SUCCESS":
      return { ...state, success: action.payload, error: null };
    case "ADD_PAYMENT_FAILURE":
      return {
        ...state,
        error: action.payload,
        success: false,
      };
    default:
      return state;
  }
};
