export const GetPaymentReducer = (state = { payments: [] }, action) => {
  switch (action.type) {
    case "GET_PAYMENTS_SUCCESS":
      return { ...state, payments: action.payload, error: null };
    case "GET_PAYMENTS_FAILURE":
      return { ...state, payments: [], loading: false, error: action.payload };
    default:
      return state;
  }
};
