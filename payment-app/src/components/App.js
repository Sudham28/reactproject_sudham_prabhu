import "./App.css";
import Banner from "./Banner";
import Navigationbar from "./Navigationbar";
import Payments from "./Payments";
import AddPayments from "./AddPayments";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { fetchPayments } from "../actions";

function App(props) {
  const [loadPayment, setLoadPayment] = useState(false);

  useEffect(() => {
    props.fetchPayments();
  }, [loadPayment, props.fetchPayments]);

  return (
    <Router>
      <div>
        <Navigationbar />
        <div>
          <Switch>
            <Route exact path="/">
              <Banner />
              <div className="container">
                <Payments />
              </div>
            </Route>
            <Route exact path="/addPayments">
              <div className="container">
                <AddPayments
                  loadPayment={loadPayment}
                  setLoadPayment={setLoadPayment}
                />
              </div>
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    payments: state.payments,
  };
};

const actionCreator = {
  fetchPayments,
};
export default connect(mapStateToProps, actionCreator)(App);
