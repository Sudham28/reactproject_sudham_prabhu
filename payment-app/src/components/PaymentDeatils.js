import { useState } from "react";
import TogglePayment from "./TogglePayment";
import PaymnetType from "./PaymentType";

const PaymentDetails = (props) => {
  const [visible, setVisible] = useState(true);
  return (
    <div>
      <div className="card mb-4 box-shadow">
        <PaymnetType type={props.payment.type} />
        {visible && (
          <div className="card-body">
            <h1 className="card-title pricing-card-title">
              {props.payment.amount} <small className="text-muted"> $</small>
            </h1>
            <ul className="list-unstyled mt-3 mb-4">
              <li>
                {new Intl.DateTimeFormat("en-GB", {
                  year: "numeric",
                  month: "long",
                  day: "2-digit",
                }).format(new Date(props.payment.paymentDate))}
              </li>
              <li> Cust id - {props.payment.custid}</li>
              <li> ID- {props.payment.id}</li>
            </ul>
          </div>
        )}
        <TogglePayment visible={visible} toggle={setVisible} />
      </div>
    </div>
  );
};
export default PaymentDetails;
