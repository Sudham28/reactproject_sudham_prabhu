import { NavLink } from "react-router-dom";
const Navigationbar = () => {
  return (
    <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 className="my-0 mr-md-auto font-weight-normal">Payment APP</h5>
      <nav className="my-2 my-md-0 mr-md-3">
        <span className="p-2 text-dark">
          <NavLink to="/">Payments</NavLink>
        </span>
        <span className="p-2 text-dark">
          <NavLink to="/addPayments">Add Payments</NavLink>
        </span>
      </nav>
    </div>
  );
};

export default Navigationbar;
