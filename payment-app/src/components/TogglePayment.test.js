import TogglePayment from "./TogglePayment";
import React from "react";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("TOGLE PAYMENT ckicked when visible is true", () => {
  const setVisible = jest.fn();
  act(() => {
    render(<TogglePayment visible={true} toggle={setVisible} />, container);
  });
  const button = document.querySelector(".btn");
  expect(button.innerHTML).toBe("Hide");
});

it("TOGLE PAYMENT ckicked when visible is false", () => {
  const setVisible = jest.fn();
  act(() => {
    render(<TogglePayment visible={false} toggle={setVisible} />, container);
  });
  const button = document.querySelector(".btn");
  expect(button.innerHTML).toBe("Show");
});
