const PaymnetType = (props) => {
  return (
    <div className="card-header">
      <h4 className="my-0 font-weight-normal">
        {props.type === "D" ? "Debit" : "Credit"}
      </h4>
    </div>
  );
};
export default PaymnetType;
