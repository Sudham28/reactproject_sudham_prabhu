// import { render, screen } from "@testing-library/react";
import PaymentType from "./PaymentType";
import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("When payment type is D", () => {
  act(() => {
    render(<PaymentType type="D" />, container);
  });
  expect(container.querySelector(".my-0").textContent).toBe("Debit");
});

it("When payment type is D", () => {
  act(() => {
    render(<PaymentType type="C" />, container);
  });
  expect(container.querySelector(".my-0").textContent).toBe("Credit");
});
