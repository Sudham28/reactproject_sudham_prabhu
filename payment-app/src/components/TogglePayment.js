const TogglePayment = (props) => {
  return (
    <button
      type="button"
      onClick={() => props.toggle(!props.visible)}
      className="btn btn-lg btn-block btn-outline-primary"
    >
      {props.visible ? "Hide" : "Show"}
    </button>
  );
};
export default TogglePayment;
