import { connect, useDispatch } from "react-redux";
import Loader from "./Loader";
import ErrorMessage from "./ErrorMessage";
import { useState } from "react";
import { addPayments, addPaymnetSuccess } from "../actions";
import { useHistory } from "react-router-dom";

const AddPayments = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [paymentDate, setPaymentDate] = useState(new Date());
  const [type, setType] = useState("D");
  const [amount, setAmount] = useState("");
  const [custid, setCustid] = useState("");
  const [id, setId] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(
      addPayments({
        id: id,
        type: type,
        amount: amount,
        custid: custid,
        paymentDate: paymentDate,
      })
    );
  };

  if (props.loader) {
    return <Loader />;
  }
  if (props.success) {
    console.log("addpayments- props.success", props.success);
    props.setLoadPayment(!props.loadPayment);
    dispatch(addPaymnetSuccess(false));
    setTimeout(() => {
      history.push("/");
    }, 2000);
  }

  return (
    <div>
      {props.error !== null && props.error !== undefined && (
        <ErrorMessage message={props.error.message} />
      )}
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-group">
          <label htmlFor="id">Id</label>
          <input
            className="form-control"
            type="number"
            id="id"
            onChange={(e) => setId(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="type">Type</label>
          <select
            value={type}
            className="form-control"
            id="type"
            onChange={(e) => {
              setType(e.target.value);
            }}
          >
            <option>D</option>
            <option>C</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="date">Date</label>
          <input
            className="form-control"
            type="date"
            id="date"
            onChange={(e) => setPaymentDate(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="amount">Amount</label>
          <input
            type="number"
            className="form-control"
            id="amount"
            onChange={(e) => setAmount(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="custid">Customer id</label>
          <input
            type="number"
            className="form-control"
            id="custid"
            onChange={(e) => setCustid(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log("mapStateToProps-addpayments", state);
  return {
    loader: state.Commons.loader,
    error: state.AddPayments.error,
    success: state.AddPayments.success,
  };
};

export default connect(mapStateToProps)(AddPayments);
