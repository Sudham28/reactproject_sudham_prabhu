import PaymentDetails from "./PaymentDeatils";

import { connect } from "react-redux";
import Loader from "./Loader";
import ErrorMessage from "./ErrorMessage";

const Payments = (props) => {
  return props.loading ? (
    <Loader />
  ) : props.error ? (
    <ErrorMessage message={props.error} />
  ) : (
    <div className="card-deck mb-3 text-center row">
      {props.payments.map((payment) => (
        <PaymentDetails key={payment.id} payment={payment} />
      ))}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    payments: state.GetPayments.payments,
    loading: state.Commons.loader,
    error: state.Commons.errorMessage,
  };
};
export default connect(mapStateToProps)(Payments);
