import PaymentDeatils from "./PaymentDeatils";
import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import PaymentType from "./PaymentType";

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("When payment amount is 123", () => {
  const payment = {
    id: 1,
    paymentDate: "2020-11-25T00:00:00.000+00:00",
    type: "D",
    amount: 123,
    custid: 23,
  };
  act(() => {
    render(<PaymentDeatils payment={payment} />, container);
  });
  expect(container.querySelector("h1").textContent).toBe("123  $");
  act(() => {
    render(<PaymentDeatils payment={payment} />, container);
  });
  expect(container.querySelector(".list-unstyled").textContent).toBe(
    "November 25, 2020 Cust id - 23 ID- 1"
  );
});
